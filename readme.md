# Project Cool Image Stuff

this is a projected dedicated to doing cool stuff with photography/imagery. Right now these are the supported features

* time lapse creation
  * example: `python main.py -m timelapse -i {INPUT_DIR} -o {OUTPUT_FILE} [--fps 24 --display True --res 1080p]`
  
  
## Getting Started
This project requires **python 3.6.#** or higher! 

> [installing py3.6+](https://realpython.com/installing-python/)
> 
> [install py3.6+ using conda](https://docs.conda.io/projects/conda/en/latest/user-guide/install/)

Steps to get started:

  1. have python 3.6+ installed!
  2. clone this repo: `git clone git clone https://dhanani94@bitbucket.org/dhanani94/project_cis.git`
  3. navigate to directory in terminal: `cd project_cis`
  4. install requirements: `pip install -r requirements.txt`
  5. run help screen for more info:  `python main.py -h `