import argparse
from src.stuff.timelapse import run_time_lapse
import logging
from src.utils.logging_util import initialise_logger

logger = logging.getLogger(__name__)


def main():
    initialise_logger()
    parser = argparse.ArgumentParser(description='Project Cool Image Stuff')
    parser.add_argument('-i', '--input', help='Input image or directory to run on', required=True, dest='input')
    parser.add_argument('-o', '--output', help='Output file or directory', default='output', dest='output',
                        required=True)
    parser.add_argument('-f', '--fps', help='Output fps', dest='fps', type=int, default=24)
    parser.add_argument('-d', '--display', help='display results realtime', dest='display', type=bool, default=False)
    parser.add_argument('-r', '--res', help='Output resolution', default='480p', type=str,
                        choices=["480p", "720p", "1080p", "4k"], dest='res')
    parser.add_argument('-m', '--mode', help='what program to run, default timelapse', type=str, choices=['timelapse'],
                        default='timelapse', dest='mode', required=True)
    args = parser.parse_args()

    if args.mode == 'timelapse':
        run_time_lapse(args.input, args.output, args.res, args.fps, args.display)


if __name__ == '__main__':
    main()
