import logging

import cv2
import numpy as np

from src.resources.constants import STD_DIMENSIONS
from src.utils.file_system_utils import get_all_images
from src.utils.image_utils import resize_image

logger = logging.getLogger(__name__)


def run_create_blended_image(img_dir, image_out, res="480p", display=False):
    dims = STD_DIMENSIONS.get(res)

    image_list = get_all_images(img_dir)
    image_sum = np.zeros([dims[1], dims[0], 3])

    total_img_count = len(image_list)
    for counter, file in enumerate(image_list, 1):
        image_frame = cv2.imread(file)
        image_frame = resize_image(image_frame, dims)
        print("processing image ({}/{})".format(counter, total_img_count))
        if display:
            cv2.imshow('image_frame_masked', image_frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                cv2.destroyAllWindows()
                return
        image_sum += image_frame
    image_sum /= total_img_count
    cv2.imwrite(image_out, image_sum)
    print("DONEZO")
