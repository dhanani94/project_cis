import logging
import os

import cv2
import numpy as np

from src.resources.constants import STD_DIMENSIONS, VIDEO_TYPE
from src.utils.file_system_utils import get_all_images
from src.utils.image_utils import resize_image, blend_images

logger = logging.getLogger(__name__)


def run_time_lapse(input_dir, output, res="480p", fps=24.0, display=False, mask_file=None):
    output = output.replace("\\", "/")
    if "." not in output:
        if output[-1] != "/":
            output += "/"
        output += "video.mp4"

    logger.info("starting time lapse process")
    logger.info("input dir: {}".format(input_dir))
    logger.info("output video: {}".format(output))

    extension = os.path.splitext(output)[1]
    dims = STD_DIMENSIONS.get(res)
    codec = VIDEO_TYPE.get(extension)
    out = cv2.VideoWriter(output, codec, fps, dims)

    # if mask_file:
    #     mask = cv2.imread(mask_file, 0)
    #     mask = resize_image(mask, dims)
    #     _, mask_remove = cv2.threshold(mask, 10, 255, cv2.THRESH_BINARY)
    #     _, mask_keep = cv2.threshold(mask, 10, 255, cv2.THRESH_BINARY_INV)
    #
    #     mask_img = cv2.imread(mask_file)
    #     mask_img = resize_image(mask_img, dims)
    #
    #     image_frame_remove = cv2.bitwise_and(mask_img, mask_img, mask=mask_remove)

    image_list = get_all_images(input_dir)
    prev_frame = None
    total_img_count = len(image_list)

    for counter, file in enumerate(image_list, 1):
        logger.info("processing image ({}/{})".format(counter, total_img_count))
        image_frame = cv2.imread(file)
        if type(prev_frame) == np.ndarray:
            image_frame_blend = blend_images(image_frame, prev_frame)
        else:
            image_frame_blend = None
        prev_frame = image_frame.copy()

        image_frame = resize_image(image_frame, dims)
        # if mask_file:
        #     image_frame_keep = cv2.bitwise_and(image_frame, image_frame, mask=mask_keep)
        #     image_frame = image_frame_keep + image_frame_remove

        if display:
            cv2.imshow('frame', image_frame)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                cv2.destroyAllWindows()
                return
        else:
            if type(image_frame_blend) == np.ndarray:
                out.write(image_frame_blend)
            out.write(image_frame)

    out.release()
    logger.info("saved video: {}".format(output))
