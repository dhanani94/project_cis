import logging

import cv2
import numpy as np

from src.resources.constants import STD_DIMENSIONS
from src.utils.file_system_utils import get_all_images
from src.utils.image_utils import resize_image

logger = logging.getLogger(__name__)


def run_create_masks(img_dir, mask_img, image_out, res="480p", display=False):
    dims = STD_DIMENSIONS.get(res)

    image_list = get_all_images(img_dir)
    mask = cv2.imread(mask_img, 0)
    mask = resize_image(mask, dims)
    _, mask = cv2.threshold(mask, 127, 255, cv2.THRESH_BINARY)

    print(mask)
    if display:
        cv2.imshow('mask', mask)
        if cv2.waitKey(1) & 0xFF == ord('q'):
            cv2.destroyAllWindows()
    image_sum = np.zeros([dims[1], dims[0], 3])
    print("image_sum: {}".format(image_sum.shape))
    total_img_count = len(image_list)
    for counter, file in enumerate(image_list, 1):
        image_frame = cv2.imread(file)
        image_frame = resize_image(image_frame, dims)
        print("processing image ({}/{})".format(counter, total_img_count))
        image_frame_masked = cv2.bitwise_and(image_frame, image_frame, mask=mask)

        if display:
            cv2.imshow('image_frame_masked', image_frame_masked)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                cv2.destroyAllWindows()
                return
        image_sum += image_frame_masked
    image_sum /= total_img_count
    cv2.imwrite(image_out, image_sum)
    print("DONEZO")
