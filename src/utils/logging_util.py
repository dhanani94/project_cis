import logging


def parse_level(level):
    if level.upper() == "DEBUG":
        return logging.DEBUG
    elif level.upper() == "INFO":
        return logging.INFO


def initialise_logger(level="DEBUG"):
    # create logger
    logger = logging.getLogger()
    logger.setLevel(parse_level(level))

    # create console handler and set level to debug
    ch = logging.StreamHandler()
    ch.setLevel(parse_level(level))

    # create formatter
    formatter = logging.Formatter('%(asctime)s [%(levelname)s] %(name)s: %(message)s')

    # add formatter to ch
    ch.setFormatter(formatter)

    # add ch to logger
    logger.addHandler(ch)
