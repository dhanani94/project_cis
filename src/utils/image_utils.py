import logging

import cv2

logger = logging.getLogger(__name__)


def resize_image(img, dims):
    return cv2.resize(img, dims)


def blend_images(imga, imgb):
    return cv2.addWeighted(imga, 0.5, imgb, 0.5, 0)
