import glob
import logging

logger = logging.getLogger(__name__)


def get_all_images(img_dir, sort=True, sort_by="name", target_ext="jpg"):
    if not target_ext:
        target_ext = ["jpg"]
    if type(target_ext) != list:
        target_ext = [target_ext]
    image_list = []
    for ext in target_ext:
        image_list.extend(glob.glob(f"{img_dir}/*.{ext}"))
    if sort:
        if sort_by is "name":
            image_list = sorted(image_list)
    return image_list
