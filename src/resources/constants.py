import logging

import cv2

logger = logging.getLogger(__name__)

STD_DIMENSIONS = {
    "480p": (640, 480),
    "720p": (1280, 720),
    "1080p": (1920, 1080),
    "4k": (3840, 2160),
}

# Types of Codes: http://www.fourcc.org/codecs.php
VIDEO_TYPE = {
    '.mp4': cv2.VideoWriter_fourcc(*'H264'),
}
